package pl.dfl.puzzlesolver.worker;

import org.junit.Test;
import pl.dfl.puzzlesolver.exception.BlocksFillAllTableFieldsException;
import pl.dfl.puzzlesolver.model.Table;

import java.util.LinkedList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class MixerTest {

    @Test
    public void simpleTest() {

        Table table = new Table(3, 3, "C", "H", "A", "R", "Y", "B", "D", "I");
        Mixer mixer = new Mixer(table, 100);

        mixer.mix(table);
    }

    @Test
    public void givenDepth_whenMixMethodDepth_thenDepthEqualsGivenValue() {
        Table givenTable = new Table(2, 2, "0", "1", "2");
        int depth = 50;

        Mixer mixer = new Mixer();
        mixer.setGivenTable(givenTable);

        mixer.mix(depth);

        assertEquals(50, mixer.getDepth());
    }

    @Test
    public void givenTableAndDepth_whenMixMethodDepthAndTable_thenDepthAndTableAreEqualsGivenValues() {
        Table givenTable = new Table(2, 2, "0", "1", "2");
        int depth = 50;

        Mixer mixer = new Mixer();

        mixer.mix(givenTable, depth);

        assertEquals(50, mixer.getDepth());
        assertEquals(new Table(2, 2, "0", "1", "2"), mixer.getGivenTable());
    }

    @Test
    public void givenTable_whenMixMethodTable_thenTableIsGivenValue() {
        Table givenTable = new Table(2, 2, "0", "1", "2");
        int depth = 50;

        Mixer mixer = new Mixer();

        mixer.mix(givenTable, depth);

        assertEquals(50, mixer.getDepth());
        assertEquals(new Table(2, 2, "0", "1", "2"), mixer.getGivenTable());
    }

    @Test
    public void givenConstMixerWithTable_thenMixersTableIsEqualsGivenTable() {
        Table givenTable = new Table(2, 2, "0", "1", "2");
//        int depth = 50;

        Mixer mixer = new Mixer(givenTable);

        assertEquals(new Table(2, 2, "0", "1", "2"), mixer.getGivenTable());
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenMixer_whenGivenTableIsNull_thenThrowIllegalArgumentException() {
        Mixer mixer = new Mixer();
        mixer.setGivenTable(null); //is't necessary

        mixer.mix();
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenMixer_whenMixingDepthIsNegative_thenThrowIllegalArgumentException() {
        Mixer mixer = new Mixer();
        mixer.setGivenTable(new Table(2, 2, "0", "1", "2"));
        mixer.setDepth(-1); //the value is't correct

        mixer.mix();
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenMixer_whenMixingDepthIs0_thenThrowIllegalArgumentException() {
        Mixer mixer = new Mixer();
        mixer.setGivenTable(new Table(2, 2, "0", "1", "2"));
        mixer.setDepth(0); //the value is't correct

        mixer.mix();
    }

    @Test(expected = BlocksFillAllTableFieldsException.class)
    public void givenMixer_whenTableIsFull_thenThrowBlocksFillAllTableFieldsException() {
        Mixer mixer = new Mixer();
        mixer.setGivenTable(new Table(2, 2, "0", "1", "2", "3"));

        mixer.mix();
    }

    @Test
    public void givenMixer_whenSimpleCase_thenAnyResultIsNotEqualsGivenTable() {
        Table initTable = new Table(2, 2, "0", "1", "2");
        Mixer mixer = new Mixer(initTable);

        LinkedList<Table> resultTables = new LinkedList<>();

        for (int i = 0; i < 50; i++) {
            mixer.mix();
            Table result = mixer.getResult();
            resultTables.add(result);
        }

        assertFalse("Results tables are contains init table? \t", resultTables.contains(initTable));
    }
}
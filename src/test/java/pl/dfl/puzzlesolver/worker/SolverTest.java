package pl.dfl.puzzlesolver.worker;

import org.junit.Test;
import pl.dfl.puzzlesolver.model.Block;
import pl.dfl.puzzlesolver.model.Position;
import pl.dfl.puzzlesolver.model.Table;

import java.util.Collection;

import static org.junit.Assert.*;

public class SolverTest {

    @Test
    public void whenCreateSolver_thenCreateStartingCombination() {

        Table startingTable = new Table(1, 2);
        startingTable.fillTableSimple();

        Table winningTable = new Table(1, 2);
        startingTable.fillTableSimple();

        Solver solver = new Solver(startingTable, winningTable);

        solver.printCombinations();
    }

    @Test
    public void whenAnyPositionInTableIsNull_thenCanDoMoveReturnTrue() {
        Table table = new Table(1, 2);
        Block block = new Block("1");

        table.setOnPosition(block, 0, 0);

        assertTrue(Solver.canDoAnyMove(table));
    }

    @Test
    public void whenAnyPositionInTableIsNotNull_thenCanDoMoveReturnFalse() {
        Table table = new Table(1, 2);
        Block block = new Block("1");

        table.setOnPosition(block, 0, 0);
        table.setOnPosition(block, 0, 1);

        assertFalse(Solver.canDoAnyMove(table));
    }

    @Test
    public void whenEveryPositionInTableIsNotNull_thenReturnEmptySet() {
        Table table = new Table(1, 2);
        Block block = new Block("1");

        table.setOnPosition(block, 0, 0);
        table.setOnPosition(block, 0, 1);

        assertEquals(true, Solver.emptyPositions(table).isEmpty());
    }

    @Test
    public void whenAnyPositionInTableIsNotNull_thenReturnNotEmptySet() {
        Table table = new Table(1, 2);
        Block block = new Block("1");

        table.setOnPosition(block, 0, 0);

        assertEquals(false, Solver.emptyPositions(table).isEmpty());
    }

    @Test
    public void whenOnePositionIn1x2TableIsNotNull_thenSizeSet1() {
        Table table = new Table(1, 2);
        Block block = new Block("1");

        table.setOnPosition(block, 0, 0);

        assertEquals(1, Solver.emptyPositions(table).size());
    }

    //moveDown()
    @Test
    public void whenEmptyPositionIsInFirstRow_thenAfterMoveDownReturnNull() {
        Table table = new Table(2, 1);
        table.setOnPosition(new Block("1"), 1, 0);

        Solver solver = new Solver();

        assertEquals(null, solver.tableAfterMovingDown(table, new Position(1, 0)));
    }

    @Test
    public void whenEmptyPositionIsNotFirstRow_thenMoveDownReturnNotNull() {
        Table table = new Table(2, 1);
        table.setOnPosition(new Block("1"), 0, 0);

        Solver solver = new Solver();

        assertNotNull(solver.tableAfterMovingDown(table, new Position(1, 0)));
    }

    @Test
    public void whenTableCanMovingDown_thenRearrangeTable() {

        Table expected = new Table(2, 1);
        expected.setOnPosition(new Block("1"), 1, 0);

        Table table = new Table(2, 1);
        table.setOnPosition(new Block("1"), 0, 0);

        Solver solver = new Solver();

        Table table1 = solver.tableAfterMovingDown(table, new Position(1, 0));
        assertEquals(expected, table1);
    }

    //moveUp()
    @Test
    public void whenEmptyPositionIsInLastRow_thenMoveUpReturnNull() {
        Table table = new Table(2, 1);
        table.setOnPosition(new Block("1"), 0, 0);

        Solver solver = new Solver();

        assertEquals(null, solver.tableAfterMovingUp(table, new Position(1, 0)));
    }

    @Test
    public void whenEmptyPositionIsNotInLastRow_thenMoveUpReturnNotNull() {
        Table table = new Table(2, 1);
        table.setOnPosition(new Block("1"), 1, 0);

        Solver solver = new Solver();
        assertNotNull(solver.tableAfterMovingUp(table, new Position(0, 0)));
    }

    @Test
    public void whenTableCanMovingUp_thenRearrangeTable() {

        Table expected = new Table(2, 1);
        expected.setOnPosition(new Block("1"), 0, 0);

        Table table = new Table(2, 1);
        table.setOnPosition(new Block("1"), 1, 0);

        Solver solver = new Solver();

        assertEquals(expected, solver.tableAfterMovingUp(table, new Position(0, 0)));
    }

    //moveLeft()
    @Test
    public void whenEmptyPositionIsInLastColumn_thenMoveLeftReturnNull() {
        Table table = new Table(1, 2);
        table.setOnPosition(new Block("1"), 0, 0);

        Solver solver = new Solver();
        assertNull(solver.tableAfterMovingLeft(table, new Position(0, 1)));
    }

    @Test
    public void whenEmptyPositionIsNotInLastColumn_thenMoveLeftReturnNotNull() {
        Table table = new Table(1, 2);
        table.setOnPosition(new Block("1"), 0, 1);

        Solver solver = new Solver();
        assertNotNull(solver.tableAfterMovingLeft(table, new Position(0, 0)));
    }

    @Test
    public void whenTableCanMovingLeft_thenRearrangeTable() {

        Table expected = new Table(1, 2);
        expected.setOnPosition(new Block("1"), 0, 0);

        Table table = new Table(1, 2);
        table.setOnPosition(new Block("1"), 0, 1);

        Solver solver = new Solver();

        assertEquals(expected, solver.tableAfterMovingLeft(table, new Position(0, 0)));
    }

    //moveRight()
    @Test
    public void whenEmptyPositionIsInFirstColumn_thenMoveLeftReturnNull() {
        Table table = new Table(1, 2);
        table.setOnPosition(new Block("1"), 0, 1);

        Solver solver = new Solver();
        assertNull(solver.tableAfterMovingRight(table, new Position(0, 0)));
    }

    @Test
    public void whenEmptyPositionIsNotInFirstColumn_thenMoveLeftReturnNotNull() {
        Table table = new Table(1, 2);
        table.setOnPosition(new Block("1"), 0, 0);

        Solver solver = new Solver();
        assertNotNull(solver.tableAfterMovingRight(table, new Position(0, 1)));
    }

    @Test
    public void whenTableCanMovingRight_thenRearrangeTable() {

        Table expected = new Table(1, 2);
        expected.setOnPosition(new Block("1"), 0, 1);

        Table table = new Table(1, 2);
        table.setOnPosition(new Block("1"), 0, 0);

        Solver solver = new Solver();

        assertEquals(expected, solver.tableAfterMovingRight(table, new Position(0, 1)));
    }

    @Test
    public void moveLeft() {
        Table table = new Table(1, 3);
        table.setOnPosition(new Block("0"), 0, 2);

        table.printTable();
        System.out.println("############################");

        Solver solver = new Solver();

        table = solver.tableAfterMovingLeft(table, new Position(0, 1));
        table.printTable();
        System.out.println("############################");

        table = solver.tableAfterMovingLeft(table, new Position(0, 0));
        table.printTable();
        System.out.println("############################");

        Table expected = new Table(1, 3);
        expected.setOnPosition(new Block("0"), 0, 0);

        assertEquals(expected, table);
    }

    @Test
    public void moveRight() {
        Table table = new Table(1, 3);
        table.setOnPosition(new Block("0"), 0, 0);

        table.printTable();
        System.out.println("############################");

        Solver solver = new Solver();

        table = solver.tableAfterMovingRight(table, new Position(0, 1));
        table.printTable();
        System.out.println("############################");

        table = solver.tableAfterMovingRight(table, new Position(0, 2));
        table.printTable();
        System.out.println("############################");

        Table expected = new Table(1, 3);
        expected.setOnPosition(new Block("0"), 0, 2);

        assertEquals(expected, table);
    }

    @Test
    public void moveUp() {
        Table table = new Table(3, 1);
        table.setOnPosition(new Block("0"), 2, 0);

        table.printTable();
        System.out.println("############################");

        Solver solver = new Solver();

        table = solver.tableAfterMovingUp(table, new Position(1, 0));
        table.printTable();
        System.out.println("############################");

        table = solver.tableAfterMovingUp(table, new Position(0, 0));
        table.printTable();
        System.out.println("############################");

        Table expected = new Table(3, 1);
        expected.setOnPosition(new Block("0"), 0, 0);

        assertEquals(expected, table);
    }

    @Test
    public void moveDown() {
        Table table = new Table(3, 1);
        table.setOnPosition(new Block("0"), 0, 0);

        table.printTable();

        Solver solver = new Solver();

        table = solver.tableAfterMovingDown(table, new Position(1, 0));
        table.printTable();

        table = solver.tableAfterMovingDown(table, new Position(2, 0));
        table.printTable();

        Table expected = new Table(3, 1);
        expected.setOnPosition(new Block("0"), 2, 0);

        assertEquals(expected, table);
    }

    @Test
    public void whenTables3x3EmptyCenter_thenGetTablesAfterMovingReturn4elementsCollection() {
        Table table = new Table(3, 3);
        table.setOnPosition(new Block("0"), 0, 0);
        table.setOnPosition(new Block("1"), 0, 1);
        table.setOnPosition(new Block("2"), 0, 2);

        table.setOnPosition(new Block("3"), 1, 0);
//        table.setOnPosition(new Block("0"), 1, 1);
        table.setOnPosition(new Block("5"), 1, 2);

        table.setOnPosition(new Block("6"), 2, 0);
        table.setOnPosition(new Block("7"), 2, 1);
        table.setOnPosition(new Block("8"), 2, 2);

        Solver solver = new Solver();
        Position emptyPosition = new Position(1, 1);

        Collection<Table> result = solver.getTablesAfterMoves(table, emptyPosition);

        for (Table t : result) {
            t.printTable();
            System.out.println();
        }

        assertEquals(4, result.size());
    }

    @Test
    public void charybdisSolverCase1_givenWinningTableHasAnotherData_thenReturnFalse() {
        Table winningTable = new Table(3, 3);
        winningTable.setOnPosition(new Block("C"), 0, 0);
        winningTable.setOnPosition(new Block("H"), 0, 1);
        winningTable.setOnPosition(new Block("A"), 0, 2);

        winningTable.setOnPosition(new Block("R"), 1, 0);
        winningTable.setOnPosition(new Block("Y"), 1, 1);
        winningTable.setOnPosition(new Block("B"), 1, 2);

        winningTable.setOnPosition(new Block("D"), 2, 0);
        winningTable.setOnPosition(new Block("I"), 2, 1);
//        winningTable.setOnPosition(new Block("S"), 2, 2);

        //CHARYBDI(S)
        Table table = new Table(3, 3);
        table.setOnPosition(new Block("C"), 0, 0);
        table.setOnPosition(new Block("H"), 0, 1);
        table.setOnPosition(new Block("A"), 0, 2);

        table.setOnPosition(new Block("R"), 1, 0);
        table.setOnPosition(new Block("Y"), 1, 1);
        table.setOnPosition(new Block("Y"), 1, 2); //!! bad data

        table.setOnPosition(new Block("D"), 2, 0);
        table.setOnPosition(new Block("I"), 2, 1);
//        table.setOnPosition(new Block("S"), 2, 2);

        Solver solver = new Solver(table, winningTable);
        boolean isSolution = solver.solve();

        solver.printSolution();
        System.out.println("Solution depth: " + solver.getSolutionDepth());

        assertEquals(false, isSolution);
        assertEquals(0, solver.getSolutionDepth());
    }

    @Test
    public void whenTables3x3EmptyCenterFirstRow_thenGetTablesAfterMovingReturn3elementsCollection() {
        Table table = new Table(3, 3);
        table.setOnPosition(new Block("0"), 0, 0);
//        table.setOnPosition(new Block("1"),0,1);
        table.setOnPosition(new Block("2"), 0, 2);

        table.setOnPosition(new Block("3"), 1, 0);
        table.setOnPosition(new Block("4"), 1, 1);
        table.setOnPosition(new Block("5"), 1, 2);

        table.setOnPosition(new Block("6"), 2, 0);
        table.setOnPosition(new Block("7"), 2, 1);
        table.setOnPosition(new Block("8"), 2, 2);

        Solver solver = new Solver();
        Position emptyPosition = new Position(0, 1);

        Collection<Table> result = solver.getTablesAfterMoves(table, emptyPosition);

        for (Table t : result) {
            t.printTable();
            System.out.println();
        }

        assertEquals(3, result.size());
    }

    @Test
    public void given1x2Table_thenIsSolution() {
        Table startingTable = new Table(1, 2);
        startingTable.setOnPosition(new Block("0"), 0, 0);

        Table winningTable = new Table(1, 2);
        winningTable.setOnPosition(new Block("0"), 0, 1);

        Solver solver = new Solver(startingTable, winningTable);

        boolean result = solver.solve();
        solver.printSolution();

        assertEquals(true, result);
    }

    @Test
    public void given1x2Table_thenIsNoSolution() {
        Table startingTable = new Table(1, 2);
        startingTable.setOnPosition(new Block("0"), 0, 0);

        Table winningTable = new Table(1, 2);
        winningTable.setOnPosition(new Block("1"), 0, 0);

        Solver solver = new Solver(startingTable, winningTable);

        boolean result = solver.solve();
        solver.printSolution();

        assertEquals(false, result);
    }

    @Test
    public void given1x3Table_thenIsSolution() {
        Table startingTable = new Table(1, 3);
        startingTable.setOnPosition(new Block("0"), 0, 0);

        Table winningTable = new Table(1, 3);
        winningTable.setOnPosition(new Block("0"), 0, 2);

        Solver solver = new Solver(startingTable, winningTable);

        boolean result = solver.solve();
        solver.printSolution();
        System.out.println("Solution depth: " + solver.getSolutionDepth());

        assertEquals(true, result);
    }

    @Test
    public void given2x2TableOneBlock_thenIsSolution() {
        Table startingTable = new Table(2, 2);
        startingTable.setOnPosition(new Block("0"), 0, 0);

        Table winningTable = new Table(2, 2);
        winningTable.setOnPosition(new Block("0"), 1, 1);

        Solver solver = new Solver(startingTable, winningTable);

        boolean result = solver.solve();
        solver.printSolution();

        assertEquals(true, result);
    }

    @Test
    public void given2x2Table3Block_thenIsSolution() {
        Table startingTable = new Table(2, 2);
        //[0] [1]
        //[2] [ ]
        startingTable.setOnPosition(new Block("0"), 0, 0);
        startingTable.setOnPosition(new Block("1"), 0, 1);
        startingTable.setOnPosition(new Block("2"), 1, 0);

        Table winningTable = new Table(2, 2);
        //[ ] [2]
        //[1] [0]
        winningTable.setOnPosition(new Block("0"), 1, 1);
        winningTable.setOnPosition(new Block("1"), 1, 0);
        winningTable.setOnPosition(new Block("2"), 0, 1);

        Solver solver = new Solver(startingTable, winningTable);

        boolean result = solver.solve();
        solver.printSolution();

        assertEquals(true, result);
    }

    @Test
    public void given1x3Table_thenNoSolution() {
        Table startingTable = new Table(1, 3);
        //[0] [1] [ ]
        startingTable.setOnPosition(new Block("0"), 0, 0);
        startingTable.setOnPosition(new Block("1"), 0, 1);

        Table winningTable = new Table(1, 3);
        //[ ] [1] [0]
        winningTable.setOnPosition(new Block("0"), 0, 2);
        winningTable.setOnPosition(new Block("1"), 0, 1);

        Solver solver = new Solver(startingTable, winningTable);

        boolean result = solver.solve();
        solver.printSolution();

        assertEquals(false, result);
    }

    @Test
    public void charybdisSolverCase2_ABI_DRH_DY() {
        Table winningTable = new Table(3, 3);
        winningTable.setOnPosition(new Block("C"), 0, 0);
        winningTable.setOnPosition(new Block("H"), 0, 1);
        winningTable.setOnPosition(new Block("A"), 0, 2);

        winningTable.setOnPosition(new Block("R"), 1, 0);
        winningTable.setOnPosition(new Block("Y"), 1, 1);
        winningTable.setOnPosition(new Block("B"), 1, 2);

        winningTable.setOnPosition(new Block("D"), 2, 0);
        winningTable.setOnPosition(new Block("I"), 2, 1);
//        winningTable.setOnPosition(new Block("S"), 2, 2);

        //CHARYBDI(S)
        Table table = new Table(3, 3);
        table.setOnPosition(new Block("A"), 0, 0);
        table.setOnPosition(new Block("B"), 0, 1);
        table.setOnPosition(new Block("I"), 0, 2);

        table.setOnPosition(new Block("D"), 1, 0);
        table.setOnPosition(new Block("R"), 1, 1);
        table.setOnPosition(new Block("H"), 1, 2);

        table.setOnPosition(new Block("C"), 2, 0);
        table.setOnPosition(new Block("Y"), 2, 1);
//        table.setOnPosition(new Block("I"), 2, 2);

        Solver solver = new Solver(table, winningTable);
        boolean isSolution = solver.solve();

        solver.printSolution();
        System.out.println("Solution depth: " + solver.getSolutionDepth());

        assertEquals(true, isSolution);
    }

    @Test
    public void charybdisSolverCase3_givenTheSameTables_thenReturnTrueAndSolutionDepth1() {
        Table winningTable = new Table(3, 3);
        winningTable.setOnPosition(new Block("C"), 0, 0);
        winningTable.setOnPosition(new Block("H"), 0, 1);
        winningTable.setOnPosition(new Block("A"), 0, 2);

        winningTable.setOnPosition(new Block("R"), 1, 0);
        winningTable.setOnPosition(new Block("Y"), 1, 1);
        winningTable.setOnPosition(new Block("B"), 1, 2);

        winningTable.setOnPosition(new Block("D"), 2, 0);
        winningTable.setOnPosition(new Block("I"), 2, 1);
//        winningTable.setOnPosition(new Block("S"), 2, 2);

        //CHARYBDI(S)
        Table table = new Table(3, 3);
        table.setOnPosition(new Block("C"), 0, 0);
        table.setOnPosition(new Block("H"), 0, 1);
        table.setOnPosition(new Block("A"), 0, 2);

        table.setOnPosition(new Block("R"), 1, 0);
        table.setOnPosition(new Block("Y"), 1, 1);
        table.setOnPosition(new Block("B"), 1, 2);

        table.setOnPosition(new Block("D"), 2, 0);
        table.setOnPosition(new Block("I"), 2, 1);
//        table.setOnPosition(new Block("S"), 2, 2);

        Solver solver = new Solver(table, winningTable);
        boolean isSolution = solver.solve();

        solver.printSolution();
        System.out.println("Solution depth: " + solver.getSolutionDepth());

        assertEquals(true, isSolution);
        assertEquals(1, solver.getSolutionDepth());
    }

    @Test
    public void charybdisSolverCase4_CDH_BRA__IY() {
        Table winningTable = new Table(3, 3);
        winningTable.setOnPosition(new Block("C"), 0, 0);
        winningTable.setOnPosition(new Block("H"), 0, 1);
        winningTable.setOnPosition(new Block("A"), 0, 2);

        winningTable.setOnPosition(new Block("R"), 1, 0);
        winningTable.setOnPosition(new Block("Y"), 1, 1);
        winningTable.setOnPosition(new Block("B"), 1, 2);

        winningTable.setOnPosition(new Block("D"), 2, 0);
        winningTable.setOnPosition(new Block("I"), 2, 1);
//        winningTable.setOnPosition(new Block("S"), 2, 2);

        //CHARYBDI(S)
        Table table = new Table(3, 3);
        table.setOnPosition(new Block("C"), 0, 0);
        table.setOnPosition(new Block("D"), 0, 1);
        table.setOnPosition(new Block("H"), 0, 2);

        table.setOnPosition(new Block("B"), 1, 0);
        table.setOnPosition(new Block("R"), 1, 1);
        table.setOnPosition(new Block("A"), 1, 2);

//        table.setOnPosition(new Block("D"), 2, 0);
        table.setOnPosition(new Block("I"), 2, 1);
        table.setOnPosition(new Block("Y"), 2, 2);

        Solver solver = new Solver(table, winningTable);
        boolean isSolution = solver.solve();

        solver.printSolution();
        System.out.println("Solution depth: " + solver.getSolutionDepth());

        assertEquals(true, isSolution);
    }
}
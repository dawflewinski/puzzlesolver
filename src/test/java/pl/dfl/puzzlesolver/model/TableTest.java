package pl.dfl.puzzlesolver.model;

import org.junit.Assert;
import org.junit.Test;
import pl.dfl.puzzlesolver.exception.BlocksFillAllTableFieldsException;
import pl.dfl.puzzlesolver.exception.ColumnNumberException;
import pl.dfl.puzzlesolver.exception.RowNumberException;
import pl.dfl.puzzlesolver.exception.ToMuchBlocksException;

import static org.junit.Assert.*;

public class TableTest {

    @Test
    public void whenSetBlockIntoTable_thenBlockIsOnPosition() {
        Table tempTable = new Table(1, 1);

        Block block = new Block("1");

        tempTable.setOnPosition(block, 0, 0);

        Assert.assertEquals(block, tempTable.getFromPosition(0, 0));
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void whenSetBlockOutsideTable_thenThrowException() {
        Table tempTable = new Table(1, 1);

        Block block = new Block("1");

        tempTable.setOnPosition(block, 1, 1);
    }

    @Test
    public void printTableAfterSimpleFilling() {
        Table table = new Table(3, 4);
        table.fillTableSimple();

        table.printTable();
    }

    @Test
    public void printTable() {
        Table table = new Table(3, 4);
        table.setOnPosition(new Block("1"), 1, 3);

        table.printTable();
    }

    @Test
    public void whenTwoTablesHaveDifferentSize_thenEqualsReturnFalse() {
        Table table1 = new Table(1, 2);
        Table table2 = new Table(2, 1);

        assertNotEquals(table1, table2);
    }

    @Test
    public void whenTwoTablesHaveTheSameSize_thenEqualsReturnTrue() {
        Table table1 = new Table(1, 2);
        Table table2 = new Table(1, 2);

        assertEquals(table1, table2);
    }

    @Test
    public void whenTableAndObject_thenEqualsReturnFalse() {
        Table table1 = new Table(1, 2);
        Object object = new Object();

        assertNotEquals(table1, object);
    }

    @Test
    public void whenTwoTablesHaveTheSameSizeAndPutTheSameBlock_thenEqualsReturnTrue() {
        Block block = new Block("1");

        Table table1 = new Table(1, 2);
        Table table2 = new Table(1, 2);

        table1.setOnPosition(block, 0, 1);
        table2.setOnPosition(block, 0, 1);

        assertEquals(table1, table2);
    }

    @Test
    public void whenTwoTablesHaveTheSameSizeAndPutTheDifferentBlockWithTheSameValues_thenEqualsReturnTrue() {
        Block block = new Block("1");
        Block block1 = new Block("1");

        Table table1 = new Table(1, 2);
        Table table2 = new Table(1, 2);

        table1.setOnPosition(block, 0, 1);
        table2.setOnPosition(block1, 0, 1);

        assertEquals(table1, table2);
    }

    @Test
    public void whenTwoTablesHaveTheSameSizeAndPutTheDifferentBlockWithTheDifferentValues_thenEqualsReturnFalse() {
        Block block = new Block("1");
        Block block1 = new Block("2");

        Table table1 = new Table(1, 2);
        Table table2 = new Table(1, 2);

        table1.setOnPosition(block, 0, 1);
        table2.setOnPosition(block1, 0, 1);

        assertNotEquals(table1, table2);
    }

    @Test(expected = RowNumberException.class)
    public void invalidRowNumberValueStringConstructor() {
        Table result = new Table(0, 1, "a", null);
    }

    @Test(expected = ColumnNumberException.class)
    public void invalidColumnNumberValueStringConstructor() {
        Table result = new Table(1, 0, "a", null);
    }

    @Test(expected = RowNumberException.class)
    public void invalidRowNumberValueBlockConstructor() {
        Table result = new Table(0, 1, new Block("a"), null);
    }

    @Test(expected = ColumnNumberException.class)
    public void invalidColumnNumberValueBlockConstructor() {
        Table result = new Table(1, 0, new Block("a"), null);
    }

    @Test(expected = ToMuchBlocksException.class)
    public void givenStringsValuesConstructor_whenToMuchItems_thenReturnException() {
        Table result = new Table(1, 2, "a", "b", "c");
    }

    @Test(expected = ToMuchBlocksException.class)
    public void givenBlocksValuesConstructor_whenToMuchItems_thenReturnException() {
        Table result = new Table(1, 2, new Block("a"), new Block("b"), new Block("c"));
    }

    @Test
    public void givenStringsValuesConstructor_whenLessItems_thenReturnTable() {
        Table result = new Table(1, 2, "a");

        System.out.print("Result table: ");
        result.printTable();

        Table expected = new Table(1, 2);
        expected.setOnPosition(new Block("a"), 0, 0);
        expected.setOnPosition(null, 0, 1);

        System.out.print("Expected table: ");
        expected.printTable();
        assertEquals(expected, result);
    }

    @Test
    public void givenBlocksValuesConstructor_whenLessItems_thenReturnTable() {
        Table result = new Table(1, 2, new Block("a"));

        System.out.print("Result table: ");
        result.printTable();

        Table expected = new Table(1, 2);
        expected.setOnPosition(new Block("a"), 0, 0);
        expected.setOnPosition(null, 0, 1);

        System.out.print("Expected table: ");
        expected.printTable();
        assertEquals(expected, result);
    }

    @Test(expected = BlocksFillAllTableFieldsException.class)
    public void givenStringsValuesConstructor_whenNumberOfBlockIsLikeTableSize_thenReturnException() {
        Table result = new Table(1, 2, "a", "b");
    }

    @Test(expected = BlocksFillAllTableFieldsException.class)
    public void givenBlocksValuesConstructor_whenNumberOfBlockIsLikeTableSize_thenReturnException() {
        Table result = new Table(1, 2, new Block("a"), new Block("b"));
    }

}
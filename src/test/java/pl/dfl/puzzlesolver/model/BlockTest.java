package pl.dfl.puzzlesolver.model;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BlockTest {

    @Test
    public void whenBlocksContentsIsNull_thenEqualsReturnTrue(){
        Block block1 = new Block();
        Block block2 = new Block();

        assertEquals(true,block1.equals(block2));
    }

    @Test
    public void whenBlocksContentsIsTheSameValue_thenEqualsReturnTrue(){
        Block block1 = new Block("1");
        Block block2 = new Block("1");

        assertEquals(true,block1.equals(block2));
    }

    @Test
    public void whenBlocksContentsIsDifferentValue_thenEqualsReturnFalse(){
        Block block1 = new Block("1");
        Block block2 = new Block("2");

        assertEquals(false,block1.equals(block2));
    }
}
package pl.dfl.puzzlesolver.builder;

import org.junit.Test;
import pl.dfl.puzzlesolver.model.Block;
import pl.dfl.puzzlesolver.model.Table;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class TableBuilderTest {

    @Test
    public void givenTableBuilder_whenStrings_thenCreateGivenTable(){

        TableBuilder tableBuilder = new TableBuilder();
        Table given = tableBuilder.setRowNum(1).setColumnNum(2).setBlocks("a",null).build();
        given.printTable();

        Table expected = new Table(1,2,"a");

        assertEquals(expected,given);
    }

    @Test
    public void givenTableBuilder_whenBlocks_thenCreateGivenTable(){

        TableBuilder tableBuilder = new TableBuilder();
        Table given = tableBuilder.setRowNum(1).setColumnNum(2).setBlocks(new Block("a")).build();
        given.printTable();

        Table expected = new Table(1,2,"a");

        assertEquals(expected,given);
    }

    @Test
    public void givenTableBuilder_whenObjects_thenCreateGivenTable(){

        TableBuilder tableBuilder = new TableBuilder();
        LocalDateTime now = LocalDateTime.now();
        Table given = tableBuilder.setRowNum(1).setColumnNum(2).setBlocks(now,null).build();
        given.printTable();

        Table expected = new Table(1,2, new Block(now));

        assertEquals(expected,given);
    }
}
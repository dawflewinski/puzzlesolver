package pl.dfl.puzzlesolver.builder;

import pl.dfl.puzzlesolver.model.Block;
import pl.dfl.puzzlesolver.model.Table;

import java.util.LinkedList;

public class TableBuilder {

    private int rowNum = 0;
    private int columnNum = 0;

    private LinkedList<Block> blocks = new LinkedList<>();

    public TableBuilder setRowNum(int rowNum) {
        this.rowNum = rowNum;
        return this;
    }

    public TableBuilder setColumnNum(int columnNum) {
        this.columnNum = columnNum;
        return this;
    }

    public TableBuilder setBlocks(String... strings) {
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] == null) {
                this.blocks.add(null);
            } else {
                this.blocks.add(new Block(strings[i]));
            }
        }
        return this;
    }

    public TableBuilder setBlocks(Block... blocks) {
        for (int i = 0; i < blocks.length; i++) {
            if (blocks[i] == null) {
                this.blocks.add(null);
            } else {
                this.blocks.add(blocks[i]);
            }
        }
        return this;
    }

    public TableBuilder setBlocks(Object... objects){
        for (int i = 0; i < objects.length; i++) {
            if (objects[i] == null) {
                this.blocks.add(null);
            } else {
                this.blocks.add(new Block(objects[i]));
            }
        }
        return this;
    }

    public Table build() {
        return new Table(rowNum, columnNum, blocks);
    }
}

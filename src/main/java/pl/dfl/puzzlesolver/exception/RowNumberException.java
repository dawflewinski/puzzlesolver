package pl.dfl.puzzlesolver.exception;

public class RowNumberException extends RuntimeException{

    public RowNumberException() {
        super("Row number can't be <= 0");
    }
}

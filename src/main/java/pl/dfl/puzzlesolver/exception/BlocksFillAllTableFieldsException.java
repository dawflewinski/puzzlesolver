package pl.dfl.puzzlesolver.exception;

public class BlocksFillAllTableFieldsException extends RuntimeException {
    public BlocksFillAllTableFieldsException() {
        super("Trying put all fields by Blocks - there have to be at least one empty field.");
    }
}

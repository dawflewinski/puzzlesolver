package pl.dfl.puzzlesolver.exception;

public class ToMuchBlocksException extends RuntimeException {

    public ToMuchBlocksException() {
        super("There is to many block to creation of a valid table");
    }

    public ToMuchBlocksException(String message) {
        super(message);
    }
}

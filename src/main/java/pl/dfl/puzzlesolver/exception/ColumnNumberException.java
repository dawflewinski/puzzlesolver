package pl.dfl.puzzlesolver.exception;

public class ColumnNumberException extends RuntimeException{

    public ColumnNumberException() {
        super("Column number can't be <= 0");
    }
}

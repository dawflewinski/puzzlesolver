package pl.dfl.puzzlesolver.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import pl.dfl.puzzlesolver.exception.BlocksFillAllTableFieldsException;
import pl.dfl.puzzlesolver.exception.ColumnNumberException;
import pl.dfl.puzzlesolver.exception.RowNumberException;
import pl.dfl.puzzlesolver.exception.ToMuchBlocksException;

import java.util.Arrays;
import java.util.LinkedList;

public class Table {

    private int rows;
    private int columns;

    private Block[][] table;

    public Table(int numRow, int numColumn, LinkedList<Block> blocksList) {
        if (!(numRow > 0)) {
            throw new RowNumberException();
        }
        if (!(numColumn > 0)) {
            throw new ColumnNumberException();
        }

        //check size of table
        int size = numRow * numColumn;

        //check if number of blocks is lower than table size or size is exactly like size value (at least one of them
        // have to be null)
        if (blocksList.size() < size) {
            //populate table by blocks ↓
            fillTableByBlockList(numRow, numColumn, blocksList);
        } else if (blocksList.size() == size) {
            //check if exist null blocks
            if (blocksList.contains(null)) {
                //populate table by blocks ↓
                fillTableByBlockList(numRow, numColumn, blocksList);
            } else {
                //if not, throw exception
                throw new BlocksFillAllTableFieldsException();
            }
        } else {
            //trying put to much blocks to table
            throw new ToMuchBlocksException();
        }
    }

    public Table(int numRow, int numColumn, Block... blocks) throws RowNumberException, ColumnNumberException,
            ToMuchBlocksException, BlocksFillAllTableFieldsException {
        if (!(numRow > 0)) {
            throw new RowNumberException();
        }
        if (!(numColumn > 0)) {
            throw new ColumnNumberException();
        }

        //check size of table
        int size = numRow * numColumn;

        //create list of blocks -> some blocks can be null(empty)
        LinkedList<Block> blockList = new LinkedList<>();
        blockList.addAll(Arrays.asList(blocks));

        //check if number of blocks is lower than table size or size is exactly like size value (at least one of them
        // have to be null)
        if (blocks.length < size) {
            //populate table by blocks ↓
            fillTableByBlockList(numRow, numColumn, blockList);
        } else if (blocks.length == size) {
            //check if exist null blocks
            if (blockList.contains(null)) {
                //populate table by blocks ↓
                fillTableByBlockList(numRow, numColumn, blockList);
            } else {
                //if not, throw exception
                throw new BlocksFillAllTableFieldsException();
            }
        } else {
            //trying put to much blocks to table
            throw new ToMuchBlocksException();
        }
    }

    public Table(int numRow, int numColumn, String... blocksContent) throws RowNumberException, ColumnNumberException,
            ToMuchBlocksException, BlocksFillAllTableFieldsException {
        if (!(numRow > 0)) {
            throw new RowNumberException();
        }
        if (!(numColumn > 0)) {
            throw new ColumnNumberException();
        }

        //check size of table
        int size = numRow * numColumn;

        //create list of blocks -> some blocks can be null(empty)
        LinkedList<Block> blockList = new LinkedList<>();
        for (String s : blocksContent) {
            blockList.add(new Block(s));
        }

        //check if number of blocks is lower than table size or size is exactly like size value (at least one of them
        // have to be null)
        if (blocksContent.length < size) {
            fillTableByBlockList(numRow, numColumn, blockList);
        } else if (blocksContent.length == size) {
            if (blockList.contains(null)) {
                //populate table by blocks ↓
                fillTableByBlockList(numRow, numColumn, blockList);
            } else {
                //if not, throw exception
                throw new BlocksFillAllTableFieldsException();
            }
        } else {
            //trying put to much blocks to table
            throw new ToMuchBlocksException();
        }
    }

    public Table(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        table = new Block[rows][columns];
    }

    public Table(Block[][] table) {
        this.table = table;
        this.rows = table.length;
        this.columns = table[0].length;
    }

    public Table(Table table) {
        this.table = table.getTable();
        this.rows = table.getTable().length;
        this.columns = table.getTable()[0].length;
    }

    public Block[][] getTable() {
        return table;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setOnPosition(Block block, int row, int column) {
        table[row][column] = block;
    }

    public Block getFromPosition(int row, int column) {
        return table[row][column];
    }

    public void fillTableSimple() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                setOnPosition(new Block(String.valueOf(j + i * columns)), i, j);
            }
        }
    }

    public void printTable() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print("[" + table[i][j] + "]" + " ");
            }
            System.out.println();
        }
    }

    private void fillTableByStringsList(int rows, int columns, LinkedList<String> stringsList) {
        this.table = new Block[rows][columns];
        this.rows = rows;
        this.columns = columns;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (!stringsList.isEmpty()) {
                    Block tempBlock = new Block(stringsList.pollFirst());
                    table[i][j] = tempBlock;
                }
            }
        }
    }

    private void fillTableByBlockList(int rows, int columns, LinkedList<Block> blocksList) {
        this.table = new Block[rows][columns];
        this.rows = rows;
        this.columns = columns;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (!blocksList.isEmpty()) {
                    Block tempBlock = blocksList.pollFirst();
                    table[i][j] = tempBlock;
                }
            }
        }
    }

    public boolean hasEmptyPosition(){
        int rows = getRows();
        int columns = getColumns();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (getFromPosition(i, j) == null) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * List of empty position in table.
     *
     * @return LinkedList of empty positions.
     */
    public LinkedList<Position> emptyPositions() {
        LinkedList<Position> result = new LinkedList<>();

        int rows = getRows();
        int columns = getColumns();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (getFromPosition(i, j) == null) {
                    result.add(new Position(i, j));
                }
            }
        }
        return result;
    }

    /**
     * Make table from given table and two position to change block.
     *
     * @param position     position to given block will be be moved.
     * @param fromPosition position from given block will be moved.
     * @return rearranged table
     */
    private Table rearrangeTable( Position position, Position fromPosition) {
        //create new table with has the same size then parent table
        Table result = new Table(getRows(), getColumns());

        //populate new table
        for (int i = 0; i < result.getRows(); i++) {
            for (int j = 0; j < result.getColumns(); j++) {
                //if position is fromPosition set null
                if (i == fromPosition.getRowNumb() && j == fromPosition.getColumnNumb()) {
                    result.setOnPosition(null, i, j);
                } else if (i == position.getRowNumb() && j == position.getColumnNumb()) {
                    //else if position then put block from fromPosition
                    result.setOnPosition(getFromPosition(fromPosition.getRowNumb(), fromPosition.getColumnNumb()), i, j);
                } else {
                    //else copy block
                    result.setOnPosition(getFromPosition(i, j), i, j);
                }
            }
        }
        return result;
    }

    /**
     * Check if on given position exist any block (Block == null).
     *
     * @param position to analise.
     * @return value of checking.
     */
    private boolean isEmptyPosition( Position position) {
        return getFromPosition(position.getRowNumb(), position.getColumnNumb()) == null;
    }

    /**
     * Make table after moving block to given position which is located above given position.
     *
     * @param position given position where given block will be move.
     * @return table after moving block to position. Return null when can't do this move.
     */
    private Table tableAfterMovingDown(Position position) {
        //position is not empty
        if (!isEmptyPosition(position)) {
            return null;
        }

        //if first row - there is nothing to move down
        if (position.getRowNumb() == 0) {
            return null;
        }

        //create position from where block will be moving
        Position fromPosition = new Position(position.getRowNumb() - 1, position.getColumnNumb());

        return rearrangeTable(position, fromPosition);
    }

    /**
     * Make table after moving block to given position which is located below given position.
     *
     * @param position given position where given block will be move.
     * @return table after moving block to position. Return null when can't do this move.
     */
    private Table tableAfterMovingUp(Position position) {
        //position is not empty
        if (!isEmptyPosition( position)) {
            return null;
        }

        //if last row - there is nothing to move up
        if (position.getRowNumb() == getRows() - 1) {
            return null;
        }

        //create position from where block will be moving
        Position fromPosition = new Position(position.getRowNumb() + 1, position.getColumnNumb());

        return rearrangeTable( position, fromPosition);
    }

    /**
     * Make table after moving block to given position which is located right side from given position.
     *
     * @param position given position where given block will be move.
     * @return table after moving block to position. Return null when can't do this move.
     */
    private Table tableAfterMovingLeft(Position position) {
        //position is not empty
        if (!isEmptyPosition( position)) {
            return null;
        }

        //if last column - there is nothing to move left
        if (position.getColumnNumb() == getColumns() - 1) {
            return null;
        }

        //create position from where block will be moving
        Position fromPosition = new Position(position.getRowNumb(), position.getColumnNumb() + 1);

        //populate new table
        return rearrangeTable(position, fromPosition);
    }

    /**
     * Make table after moving block to given position which is located left side from given position.
     *
     * @param position given position where given block will be move.
     * @return table after moving block to position. Return null when can't do this move.
     */
    private Table tableAfterMovingRight( Position position) {
        //position is not empty
        if (!isEmptyPosition( position)) {
            return null;
        }

        //if first column - there is nothing to move right
        if (position.getColumnNumb() == 0) {
            return null;
        }

        //create position from where block will be moving
        Position fromPosition = new Position(position.getRowNumb(), position.getColumnNumb() - 1);

        //populate new table
        return rearrangeTable( position, fromPosition);
    }

    /**
     * Make LinkedList of available tables after moves down, right, up, left in given position.
     *
     * @param position given position to analise.
     * @return collection of tables after moves. Can be empty when moves are not available.
     */
    public LinkedList<Table> getTablesAfterMoves(Position position) {
        LinkedList<Table> tablesAfterMoving = new LinkedList<>();

        //check possibility of moves: down, right, up, left
        // and return tables after those moves
        if (tableAfterMovingDown(position) != null) {
            tablesAfterMoving.add(tableAfterMovingDown(position));
        }
        if (tableAfterMovingRight(position) != null) {
            tablesAfterMoving.add(tableAfterMovingRight(position));
        }
        if (tableAfterMovingUp(position) != null) {
            tablesAfterMoving.add(tableAfterMovingUp( position));
        }
        if (tableAfterMovingLeft( position) != null) {
            tablesAfterMoving.add(tableAfterMovingLeft( position));
        }

        return tablesAfterMoving;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Table table1 = (Table) o;

        return new EqualsBuilder()
                .append(rows, table1.rows)
                .append(columns, table1.columns)
                .append(table, table1.table)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(rows)
                .append(columns)
                .append(table)
                .toHashCode();
    }
}

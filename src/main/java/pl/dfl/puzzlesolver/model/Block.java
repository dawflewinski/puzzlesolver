package pl.dfl.puzzlesolver.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class Block {

    private Object content;

    public Object getContent() {
        return content;
    }

    public Block() {
    }

    public Block(Object content) {
        this.content = content;
    }

    public Block(String value){
        this.content = value;
    }

    @Override
    public String toString() {
        return content.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Block block = (Block) o;

        return new EqualsBuilder()
                .append(content, block.content)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(content)
                .toHashCode();
    }
}

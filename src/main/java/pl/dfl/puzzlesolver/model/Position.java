package pl.dfl.puzzlesolver.model;

public class Position {

    /**
     * Row number where given position placed.
     */
    private int rowNumb;

    /**
     * Column number where given position placed.
     */
    private int columnNumb;

    public Position(int rowNumb, int columnNumb) {
        this.rowNumb = rowNumb;
        this.columnNumb = columnNumb;
    }

    public int getRowNumb() {
        return rowNumb;
    }

    public void setRowNumb(int rowNumb) {
        this.rowNumb = rowNumb;
    }

    public int getColumnNumb() {
        return columnNumb;
    }

    public void setColumnNumb(int columnNumb) {
        this.columnNumb = columnNumb;
    }
}

package pl.dfl.puzzlesolver.worker;

import pl.dfl.puzzlesolver.exception.BlocksFillAllTableFieldsException;
import pl.dfl.puzzlesolver.model.Position;
import pl.dfl.puzzlesolver.model.Table;

import java.util.LinkedList;
import java.util.Random;

public class Mixer {

    private Table givenTable = null;
    private Table result = null;

    private int depth = 100;

    public void setGivenTable(Table givenTable) {
        this.givenTable = givenTable;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public Table getGivenTable() {
        return givenTable;
    }

    public Table getResult() {
        return result;
    }

    public int getDepth() {
        return depth;
    }

    public Mixer() {

    }

    public Mixer(Table givenTable) {
        this.givenTable = givenTable;
    }

    public Mixer(Table givenTable, int depth) {
        this.givenTable = givenTable;
        this.depth = depth;
    }

    public void mix() {
        mix(givenTable);
    }

    public void mix(int depth) {
        this.depth = depth;
        mix(givenTable);
    }

    /**
     * Mixes the given table (100 moves by default)
     *
     * @param table given to mix.
     */
    public void mix(Table table) {
        this.givenTable = table;
        Random random = new Random();

        //when fields are null, return Exception
        if (givenTable == null || depth <= 0) {
            throw new IllegalArgumentException("Table or depth of mixing is't correct.");
        }

        //check if the table is correct
        if (!givenTable.hasEmptyPosition()) {
            throw new BlocksFillAllTableFieldsException();
        }

        //do moves
        //init work table
        Table workTable = givenTable;

        //get work table
        for (int i = 0; i < depth; i++) {
            //get empty positions
            LinkedList<Position> positionsList = workTable.emptyPositions();

            //for random position get one move (up,right,down,left)
            Position randomEmptyPos = positionsList.get(random.nextInt(positionsList.size()));

            //get tables after moves
            LinkedList<Table> tablesAfterMoves = new LinkedList<Table>();
            tablesAfterMoves.addAll(workTable.getTablesAfterMoves(randomEmptyPos));

            //set new table to work table
            workTable = tablesAfterMoves.get(random.nextInt(tablesAfterMoves.size()));
        }

        //set new table to result
        //if result table is equals given table -> do mixing once more
        if (workTable.equals(givenTable)) {
            mix();
        } else {
            this.result = workTable;
        }

        System.out.println("########Table after mixing############");
        result.printTable();
        System.out.println("######################################");
    }

    public void mix(Table initTable, int depth) {
        this.givenTable = initTable;
        this.depth = depth;
        mix(this.givenTable);
    }
}

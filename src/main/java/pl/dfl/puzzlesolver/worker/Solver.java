package pl.dfl.puzzlesolver.worker;

import pl.dfl.puzzlesolver.builder.TableBuilder;
import pl.dfl.puzzlesolver.model.Block;
import pl.dfl.puzzlesolver.model.Position;
import pl.dfl.puzzlesolver.model.Table;

import java.util.*;

public class Solver {

    /**
     * Set of used tables when solution was searching.
     */
    private volatile Set<Table> usedTables;

    /**
     * Temporary list of actual tables which can be used to searching solution.
     * <p>
     * Important: solutionDepth value can't be bigger than last combination size.
     */
    private LinkedList<LinkedList<Table>> combinations = new LinkedList<LinkedList<Table>>();

    /**
     * Parameter which describe how many tables include a potential the best solution.
     * <p>
     * 0 - no solution - bad init parameters
     * >0 - initial parameters was correct - solution exist or not.
     */
    private int solutionDepth = 0;

    /**
     * List of tables includes combination of tables from starting table to winning table.
     */
    private LinkedList<Table> solution;

    /**
     * Initial table.
     */
    private Table startingTable;

    /**
     * Table which is the winning condition.
     */
    private Table winningTable;

    public LinkedList<Table> getSolution() {
        return solution;
    }

    public void setSolution(LinkedList<Table> solution) {
        this.solution = solution;
    }

    public int getSolutionDepth() {
        return solutionDepth;
    }

    public Solver(Table startingTable, Table winningTable) {
        this.usedTables = new HashSet<>();

        this.startingTable = startingTable;
        this.winningTable = winningTable;

        this.combinations.add(new LinkedList<>(Arrays.asList(startingTable)));
    }

    public Solver() {

    }

    public List<LinkedList<Table>> getCombinations() {
        return combinations;
    }

    public void printCombination(LinkedList<Table> combination) {
        int tableNumber = 0;

        for (Table t : combination) {
            System.out.println("Table number " + tableNumber);
            System.out.println("-------------------------------");
            t.printTable();
            System.out.println("-------------------------------");
            tableNumber++;
        }
    }

    public void printCombinations() {
        int listNumber = 0;
        for (List<Table> l : this.getCombinations()) {
            System.out.println("#################################################");
            System.out.println("For " + listNumber + " list in combinations list:");
            System.out.println("#################################################");
            for (Table t : l) {
                System.out.println("-------------------------------");
                t.printTable();
                System.out.println("-------------------------------");
            }
            listNumber++;
        }
    }

    /**
     * Print all tables which are in solution list.
     */
    public void printSolution() {

        System.out.println("#############  Printing solution  ##############");
        if (solution != null) {
            for (Table t : solution) {
                System.out.println("-------------------------------");
                t.printTable();
                System.out.println("-------------------------------");
            }
        } else {
            System.out.println("There is not any solution");
        }

        System.out.println("################################################");
    }

    /**
     * Check whether is possibility of moves (empty position exist).
     *
     * @param table to analise.
     * @return possibility of moves.
     */
    static boolean canDoAnyMove(Table table) {
        int rows = table.getRows();
        int columns = table.getColumns();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (table.getFromPosition(i, j) == null) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * List of empty position in table.
     *
     * @param table to analise.
     * @return LinkedList of empty positions.
     */
    static LinkedList<Position> emptyPositions(Table table) {
        LinkedList<Position> result = new LinkedList<>();

        int rows = table.getRows();
        int columns = table.getColumns();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (table.getFromPosition(i, j) == null) {
                    result.add(new Position(i, j));
                }
            }
        }

        return result;
    }

    /**
     * Reset settings and variables in this solver.
     */
    public void reset() {
        startingTable = null;
        winningTable = null;

        resetWorkersCollection();
    }

    /**
     * Nulling collections in solver.
     */
    private void resetWorkersCollection() {
        usedTables = null;
        combinations = null;

        solution = null;
    }

    /**
     * @return when have solution return true, else return false.
     */
    public boolean solve() {
        //check whether starting and winning conditions set
        if (startingTable == null || winningTable == null) {
            return false;
        }

        //check whether starting and winning tables have the same size
        if (startingTable.getRows() != winningTable.getRows() || startingTable.getColumns() != winningTable.getColumns()) {
            return false;
        }

        //check if those tables contains the same elements
        //create set of blocks in 1. table
        Set<Block> startingTableContent = new HashSet<>();
        for (int i = 0; i < startingTable.getRows(); i++) {
            for (int j = 0; j < startingTable.getColumns(); j++) {
                startingTableContent.add(startingTable.getFromPosition(i, j));
            }
        }
        //create set of blocks in 2. table
        Set<Block> winningTableContent = new HashSet<>();
        for (int i = 0; i < winningTable.getRows(); i++) {
            for (int j = 0; j < winningTable.getColumns(); j++) {
                winningTableContent.add(winningTable.getFromPosition(i, j));
            }
        }
        //compare these lists
        if (!startingTableContent.equals(winningTableContent)) {
            return false;
        }

        //check whether the startingTable is also winningTable
        if (startingTable.equals(winningTable)) {
            //set solution
            if (solution == null) {
                solution = new LinkedList<>();
            }
            solution.add(startingTable);

            //set depth of solution
            solutionDepth = 1;

            //is solution -> return true
            return true;
        } else {
            //PREPARE WORKSPACE
            //put statingTable to combination list
            LinkedList<Table> firstCombination = new LinkedList<>();
            firstCombination.addLast(startingTable);
            usedTables.add(startingTable);
            combinations = new LinkedList<>();
            combinations.add(firstCombination);

            while (!combinations.isEmpty()) {
                //get first element from combination list and remove it from list
                LinkedList<Table> combination = combinations.pollFirst();

                //get last table from combination
                Table lastTable = null;
                if (combination != null) {
                    lastTable = combination.getLast();
                }

                LinkedList<Position> emptyPositions = null;
                if (lastTable != null) {
                    emptyPositions = emptyPositions(lastTable);
                }

                LinkedList<Table> tablesAfterMoves = new LinkedList<>();

                while (!emptyPositions.isEmpty()) {
                    System.out.println("Number of empty positions:" + emptyPositions.size());
                    Position p = emptyPositions.poll();
                    System.out.println(p.getRowNumb() + " " + p.getColumnNumb());

                    tablesAfterMoves.addAll(getTablesAfterMoves(lastTable, p));
                }


                while (!tablesAfterMoves.isEmpty()) {
                    Table tempTable = tablesAfterMoves.poll();
                    System.out.println("############ printing temp table #########");
                    tempTable.printTable();
                    System.out.println("#########################################");

                    System.out.println("usedTables contains temp table: " + usedTables.contains(tempTable));

                    if (!usedTables.contains(tempTable)) {
                        //create combination by initial combination and temp table
                        LinkedList<Table> listToAdd = new LinkedList<>();
                        listToAdd.addAll(combination);
                        listToAdd.addLast(tempTable);

                        combinations.add(listToAdd);

                        //add used table to list
                        usedTables.add(tempTable);

                        if (tempTable.equals(winningTable)) {
                            solution = listToAdd;
                            solutionDepth = listToAdd.size();
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }

    /**
     * Check if on given position exist any block (Block == null).
     *
     * @param table    to analise.
     * @param position to analise.
     * @return value of checking.
     */
    private boolean isEmptyPosition(Table table, Position position) {
        return table.getFromPosition(position.getRowNumb(), position.getColumnNumb()) == null;
    }

    /**
     * Make table from given table and two position to change block.
     *
     * @param table        to analise.
     * @param position     position to given block will be be moved.
     * @param fromPosition position from given block will be moved.
     * @return rearranged table
     */
    Table rearrangeTable(Table table, Position position, Position fromPosition) {
        //create new table with has the same size then parent table
        Table result = new Table(table.getRows(), table.getColumns());

        //populate new table
        for (int i = 0; i < result.getRows(); i++) {
            for (int j = 0; j < result.getColumns(); j++) {
                //if position is fromPosition set null
                if (i == fromPosition.getRowNumb() && j == fromPosition.getColumnNumb()) {
                    result.setOnPosition(null, i, j);
                } else if (i == position.getRowNumb() && j == position.getColumnNumb()) {
                    //else if position then put block from fromPosition
                    result.setOnPosition(table.getFromPosition(fromPosition.getRowNumb(), fromPosition.getColumnNumb()), i, j);
                } else {
                    //else copy block
                    result.setOnPosition(table.getFromPosition(i, j), i, j);
                }
            }
        }
        return result;
    }

    /**
     * Make table after moving block to given position which is located above given position.
     *
     * @param table    given table.
     * @param position given position where given block will be move.
     * @return table after moving block to position. Return null when can't do this move.
     */
    Table tableAfterMovingDown(Table table, Position position) {
        //position is not empty
        if (!isEmptyPosition(table, position)) {
            return null;
        }

        //if first row - there is nothing to move down
        if (position.getRowNumb() == 0) {
            return null;
        }

        //create position from where block will be moving
        Position fromPosition = new Position(position.getRowNumb() - 1, position.getColumnNumb());

        return rearrangeTable(table, position, fromPosition);
    }

    /**
     * Make table after moving block to given position which is located below given position.
     *
     * @param table    given table.
     * @param position given position where given block will be move.
     * @return table after moving block to position. Return null when can't do this move.
     */
    Table tableAfterMovingUp(Table table, Position position) {
        //position is not empty
        if (!isEmptyPosition(table, position)) {
            return null;
        }

        //if last row - there is nothing to move up
        if (position.getRowNumb() == table.getRows() - 1) {
            return null;
        }

        //create position from where block will be moving
        Position fromPosition = new Position(position.getRowNumb() + 1, position.getColumnNumb());

        return rearrangeTable(table, position, fromPosition);
    }

    /**
     * Make table after moving block to given position which is located right side from given position.
     *
     * @param table    given table.
     * @param position given position where given block will be move.
     * @return table after moving block to position. Return null when can't do this move.
     */
    Table tableAfterMovingLeft(Table table, Position position) {
        //position is not empty
        if (!isEmptyPosition(table, position)) {
            return null;
        }

        //if last column - there is nothing to move left
        if (position.getColumnNumb() == table.getColumns() - 1) {
            return null;
        }

        //create position from where block will be moving
        Position fromPosition = new Position(position.getRowNumb(), position.getColumnNumb() + 1);

        //populate new table
        return rearrangeTable(table, position, fromPosition);
    }

    /**
     * Make table after moving block to given position which is located left side from given position.
     *
     * @param table    given table.
     * @param position given position where given block will be move.
     * @return table after moving block to position. Return null when can't do this move.
     */
    Table tableAfterMovingRight(Table table, Position position) {
        //position is not empty
        if (!isEmptyPosition(table, position)) {
            return null;
        }

        //if first column - there is nothing to move right
        if (position.getColumnNumb() == 0) {
            return null;
        }

        //create position from where block will be moving
        Position fromPosition = new Position(position.getRowNumb(), position.getColumnNumb() - 1);

        //populate new table
        return rearrangeTable(table, position, fromPosition);
    }

    /**
     * Make LinkedList of available tables after moves down, right, up, left in given position.
     *
     * @param table    given table to analise.
     * @param position given position to analise.
     * @return collection of tables after moves. Can be empty when moves are not available.
     */
    LinkedList<Table> getTablesAfterMoves(Table table, Position position) {
        LinkedList<Table> tablesAfterMoving = new LinkedList<>();

        //check possibility of moves: down, right, up, left
        // and return tables after those moves
        if (tableAfterMovingDown(table, position) != null) {
            tablesAfterMoving.add(tableAfterMovingDown(table, position));
        }
        if (tableAfterMovingRight(table, position) != null) {
            tablesAfterMoving.add(tableAfterMovingRight(table, position));
        }
        if (tableAfterMovingUp(table, position) != null) {
            tablesAfterMoving.add(tableAfterMovingUp(table, position));
        }
        if (tableAfterMovingLeft(table, position) != null) {
            tablesAfterMoving.add(tableAfterMovingLeft(table, position));
        }

        return tablesAfterMoving;
    }

    /**
     * Make collection from previous moves (collection) and collection of tables (available moves).
     * <p>
     * When given table from tables after move was analysed before (usedTables contains given tables), do nothing.
     * This method can return empty collection. That means - there can't make any new list combination of tables.
     *
     * @param previousMoves   list from combination list which will be initial collection.
     * @param tablesAfterMove list of tables (for eg. available tables after moves for last table from previous moves
     *                        collection).
     * @return new collection (previous moves + available tables after moves).
     */
    LinkedList<LinkedList<Table>> getCombinationsAfterMoving(LinkedList<Table> previousMoves,
                                                             Collection<Table> tablesAfterMove) {
        LinkedList<LinkedList<Table>> movesAfterMoves = new LinkedList<>();

        for (Table t : tablesAfterMove) {
            if (usedTables.contains(t)) {
                //if t is in used tables do nothing
                System.out.println("############################################");
                System.out.println("Table is in used tables: ");
                t.printTable();
                System.out.println("############################################");
            } else {
                //else create new moving list and add to moves after moves
                LinkedList<Table> temp = new LinkedList<>(previousMoves);
                temp.addLast(t);

                movesAfterMoves.add(temp);
            }
        }

        return movesAfterMoves;
    }

}
